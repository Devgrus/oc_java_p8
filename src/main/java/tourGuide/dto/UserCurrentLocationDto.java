package tourGuide.dto;

import gpsUtil.location.Location;

import java.util.UUID;

public class UserCurrentLocationDto {
    private UUID userId;
    private Location lastLocation;

    public UserCurrentLocationDto() {}

    public UserCurrentLocationDto(UUID userId, Location lastLocation) {
        this.userId = userId;
        this.lastLocation = lastLocation;
    }

    public UUID getUserId() {
        return userId;
    }

    public Location getLastLocation() {
        return lastLocation;
    }
}
