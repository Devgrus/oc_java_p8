package tourGuide.service;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.*;
import java.util.concurrent.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import gpsUtil.GpsUtil;
import gpsUtil.location.Location;
import gpsUtil.location.VisitedLocation;
import tourGuide.dto.NearbyAttractionDto;
import tourGuide.dto.UserPreferencesDto;
import tourGuide.helper.InternalTestHelper;
import tourGuide.model.user.UserPreferences;
import tourGuide.tracker.Tracker;
import tourGuide.model.user.User;
import tourGuide.model.user.UserReward;
import tripPricer.Provider;
import tripPricer.TripPricer;

@Service
public class TourGuideService {
	private final Logger logger = LoggerFactory.getLogger(TourGuideService.class);
	private final GpsUtil gpsUtil;
	private final RewardsService rewardsService;
	private final TripPricer tripPricer = new TripPricer();
	public final Tracker tracker;
	boolean testMode = true;
	private final ExecutorService executorService = new ThreadPoolExecutor(40,
			50,
			10*60,
			TimeUnit.SECONDS,
			new LinkedBlockingQueue<Runnable>());
	
	public TourGuideService(GpsUtil gpsUtil, RewardsService rewardsService) {
		this.gpsUtil = gpsUtil;
		this.rewardsService = rewardsService;

		if(testMode) {
			logger.info("TestMode enabled");
			logger.debug("Initializing users");
			initializeInternalUsers();
			logger.debug("Finished initializing users");
		}
		tracker = new Tracker(this);
		addShutDownHook();
	}

	/**
	 * Get user rewards
	 * @param user user
	 * @return user rewards list
	 */
	public List<UserReward> getUserRewards(User user) {
		return user.getUserRewards();
	}

	/**
	 * get User location
	 * @param user user
	 * @return user's visited location
	 */
	public VisitedLocation getUserLocation(User user) {
		VisitedLocation visitedLocation = null;
		try {
			visitedLocation = (user.getVisitedLocations().size() > 0) ?
					user.getLastVisitedLocation() :
					trackUserLocation(user).get();
		} catch (ExecutionException | InterruptedException e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
		return visitedLocation;
	}

	/**
	 * get user
	 * @param userName username
	 * @return user information
	 */
	public User getUser(String userName) {
		return internalUserMap.get(userName);
	}

	/**
	 * Get all users
	 * @return user list
	 */
	public List<User> getAllUsers() {
		return internalUserMap.values().stream().collect(Collectors.toList());
	}

	/**
	 * Add user
	 * @param user user
	 */
	public void addUser(User user) {
		if(!internalUserMap.containsKey(user.getUserName())) {
			internalUserMap.put(user.getUserName(), user);
		}
	}

	/**
	 * Get trip deals
	 * @param user user
	 * @return attraction information list
	 */
	public List<Provider> getTripDeals(User user) {
		int cumulativeRewardPoints = user.getUserRewards().stream().mapToInt(UserReward::getRewardPoints).sum();
		List<Provider> providers = tripPricer.getPrice(tripPricerApiKey, user.getUserId(), user.getUserPreferences().getNumberOfAdults(), 
				user.getUserPreferences().getNumberOfChildren(), user.getUserPreferences().getTripDuration(), cumulativeRewardPoints);
		user.setTripDeals(providers);
		return providers;
	}

	/**
	 * Update user preferences
	 * @param user user
	 * @param dto user preferences
	 * @return user preferences
	 */
	public UserPreferencesDto updateUserPreferences(User user, UserPreferencesDto dto) {
		if(user == null || dto == null) {
			return null;
		}
		UserPreferences userPreferences = user.getUserPreferences();
		userPreferences.setNumberOfAdults(dto.getNumberOfAdults());
		userPreferences.setNumberOfChildren(dto.getNumberOfChildren());
		userPreferences.setTripDuration(dto.getTripDuration());

		return new UserPreferencesDto(dto.getUserName(),
				userPreferences.getNumberOfAdults(),
				userPreferences.getNumberOfChildren(),
				userPreferences.getTripDuration());

	}

	/**
	 * get executorService for thread management
	 * @return executorService
	 */
	public ExecutorService getExecutorService() {
		return executorService;
	}

	/**
	 * track user location
	 * @param user user
	 * @return visited location
	 */
	public Future<VisitedLocation> trackUserLocation(User user) {
		return executorService.submit(() -> {
			VisitedLocation visitedLocation = gpsUtil.getUserLocation(user.getUserId());
			user.addToVisitedLocations(visitedLocation);
			rewardsService.calculateRewards(user);
			return visitedLocation;
		});
	}

	/**
	 * Get all current user locations
	 * @return HashMap : key = userId, value = last visited location
	 */
	public HashMap<String, Location> getAllCurrentLocations() {
		HashMap<String, Location> res = new HashMap<>();
		List<User> allUsers = getAllUsers();
		allUsers.stream().filter(i -> i.getVisitedLocations().size() > 0)
				.forEach(i -> res.put(i.getUserId().toString(), i.getLastVisitedLocation().location));

		return res;
	}

	/**
	 *
	 * @param visitedLocation user visited location
	 * @return attractions list
	 */
	public List<NearbyAttractionDto> getNearbyAttractions(VisitedLocation visitedLocation) {
		int attractionCount = 5;

		return gpsUtil.getAttractions().stream()
				.sorted(Comparator.comparing(i -> rewardsService.getDistance(i, visitedLocation.location))) // sort by distance between visitedLocation and attraction
				.limit(attractionCount)
				.parallel().map(attraction -> new NearbyAttractionDto(
						attraction.attractionName, // attractionName
						attraction, // attractionLocation
						visitedLocation.location, // userLocation
						rewardsService.getDistance(attraction, visitedLocation.location),
						rewardsService.getRewardPoints(attraction.attractionId, visitedLocation.userId)
				)).collect(Collectors.toList());
	}
	
	private void addShutDownHook() {
		Runtime.getRuntime().addShutdownHook(new Thread() { 
		      public void run() {
		        tracker.stopTracking();
		      } 
		    }); 
	}
	
	/**********************************************************************************
	 * 
	 * Methods Below: For Internal Testing
	 * 
	 **********************************************************************************/
	private static final String tripPricerApiKey = "test-server-api-key";
	// Database connection will be used for external users, but for testing purposes internal users are provided and stored in memory
	private final Map<String, User> internalUserMap = new HashMap<>();
	private void initializeInternalUsers() {
		IntStream.range(0, InternalTestHelper.getInternalUserNumber()).forEach(i -> {
			String userName = "internalUser" + i;
			String phone = "000";
			String email = userName + "@tourGuide.com";
			User user = new User(UUID.randomUUID(), userName, phone, email);
			generateUserLocationHistory(user);
			
			internalUserMap.put(userName, user);
		});
		logger.debug("Created " + InternalTestHelper.getInternalUserNumber() + " internal test users.");
	}
	
	private void generateUserLocationHistory(User user) {
		IntStream.range(0, 3).forEach(i-> {
			user.addToVisitedLocations(new VisitedLocation(user.getUserId(), new Location(generateRandomLatitude(), generateRandomLongitude()), getRandomTime()));
		});
	}
	
	private double generateRandomLongitude() {
		double leftLimit = -180;
	    double rightLimit = 180;
	    return leftLimit + new Random().nextDouble() * (rightLimit - leftLimit);
	}
	
	private double generateRandomLatitude() {
		double leftLimit = -85.05112878;
	    double rightLimit = 85.05112878;
	    return leftLimit + new Random().nextDouble() * (rightLimit - leftLimit);
	}
	
	private Date getRandomTime() {
		LocalDateTime localDateTime = LocalDateTime.now().minusDays(new Random().nextInt(30));
	    return Date.from(localDateTime.toInstant(ZoneOffset.UTC));
	}
	
}
